#include "GrapplingHook.h"



GrapplingHook::GrapplingHook()
{
	arrivalDist = 0.8f;
	_path.push_back(new CHVector(0, -2.2, 0));
	_path.push_back(new CHVector(4.8,0 , 0));
	_path.push_back(new CHVector( -4.8,0, 0));
	_path.push_back(new CHVector(0, 2.2, 0));
	_currentTarget = 0;
}


GrapplingHook::~GrapplingHook()
{
}

SteeringForce GrapplingHook::Update(AIObject * thisObj, vector<AIObject*> target)
{
	SteeringForce force(0, CHVector(0,0,0), CHVector(0,0,0), 0);

	for (size_t i = 0; i < _path.size(); i++)
	{
		if (!thisObj->GetHookedState() && (thisObj->GetPos() - *_path[i]).Length() < 0.5f)
		{
			thisObj->SetHookedTarget(i);
			thisObj->SetHooked(true);
		}
	}
	if (!thisObj->GetHookedState()) return force;

	else
	{
		float toTarget = (thisObj->GetPos() - *_path[thisObj->GetHookedTarget()]).Length();
		if (toTarget < arrivalDist)
		{
			if (thisObj->GetHookedTarget() < _path.size() - 1)
			{
				thisObj->SetHookedTarget(thisObj->GetHookedTarget() + 1);
			}
			else
			{
				thisObj->SetHookedTarget(0);
				thisObj->SetHooked(false);
			}
		}

		force._linear = (*_path[thisObj->GetHookedTarget()] - thisObj->GetPos()).Normal() * thisObj->GetKninematik()->GetMaxForce();

	}
	return force;

}
