#pragma once
#include "SteeringBehaviour.h"
#include "Cohersion.h"
#include "Seperation.h"
#include "VelocityMatching.h"

class Flocking : public SteeringBehaviour
{
public:
	Flocking(vector<AIObject*> buddies);
	~Flocking();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
private:
	float _weightCoher;
	float _weightSeper;
	float _weightVeloc;

	Cohersion* _cohersion;
	Seperation* _seperation;
	VelocityMatching* _matching;
	vector<AIObject*> buddies;

};

