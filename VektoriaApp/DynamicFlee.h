#pragma once
#include "SteeringBehaviour.h"

class DynamicFlee : public SteeringBehaviour{
public:
	DynamicFlee();
	~DynamicFlee();
	SteeringForce Update(AIObject * thisObj, vector<AIObject *> targets);
};

