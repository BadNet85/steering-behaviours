#pragma once
#include "AIObject.h"
#include "Vektoria/Root.h"

using namespace Vektoria;
using namespace std;

class AIObject;
enum BehaviourType {
	FLEE,
	SEEK,
	ARRIVE,
	D_FLEE,
	D_WANDER,
	D_ARRIVE,
	D_FLOCKING
};

class SteeringBehaviour 
{
public:
	SteeringBehaviour();
	~SteeringBehaviour();
	virtual SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
	bool visable(CHVector &orientation, CHVector & destination, float FOV);
};

