#include "Cohersion.h"



Cohersion::Cohersion() 
{
	_actiavtionRadius = 1.75f;

}


Cohersion::~Cohersion() {}

SteeringForce Cohersion::Update(AIObject * thisObj, vector<AIObject*> target) {

	SteeringForce force = SteeringForce(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);

	if (target[0] == nullptr) return force;

	CHVector focusPoint = CHVector();
	int numberofactiveboids= 0;

	for (size_t i = 0; i < target.size(); i++) {
		CHVector direction = thisObj->GetPos() - target.at(i)->GetPos();
		float distance = direction.Length();
		if (distance < _actiavtionRadius) {
			focusPoint += target.at(i)->GetPos();
			numberofactiveboids++;
		}
	}

	if (numberofactiveboids > 0) {
		focusPoint /= numberofactiveboids;
		CHVector direction = focusPoint - thisObj->GetPos();
		float linearForce = thisObj->GetKninematik()->GetMaxForce()*direction.Length() / _actiavtionRadius;
		direction = direction.Normal() * linearForce;
		force._linear = direction - thisObj->GetKninematik()->GetSpeed();
		force._linear /= 0.25;
	}
	return force;
}
