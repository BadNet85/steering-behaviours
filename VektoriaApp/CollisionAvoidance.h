#pragma once
#include "SteeringBehaviour.h"
#include "DynamicFlee.h"

class CollisionAvoidance : public SteeringBehaviour
{
private:
	DynamicFlee _flee;

public:
	CollisionAvoidance();
	~CollisionAvoidance();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
};

