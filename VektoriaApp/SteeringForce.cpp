#include "SteeringForce.h"



SteeringForce::SteeringForce(float angular, CHVector linear, CHVector kinematikLinear, float kinematikAngular)
{
	_angular = angular;
	_linear = linear;
	_kinematikLinear = kinematikLinear;
	_kinematikAngular = kinematikAngular;
}


SteeringForce::~SteeringForce() {}
