#pragma once
#include "SteeringBehaviour.h"

class VelocityMatching : public SteeringBehaviour{
public:
	VelocityMatching();
	~VelocityMatching();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
private:
	float _activationRadius;
};

