#pragma once
#include "Vektoria/Root.h"

using namespace Vektoria;

class SteeringForce {
public:
	SteeringForce(float angular, CHVector linear, CHVector kinematikLinear, float kinematikAngular);
	~SteeringForce();
	float _angular;
	CHVector _linear;
	CHVector _kinematikLinear;
	float _kinematikAngular;
	float _angularChange = 0;

};

