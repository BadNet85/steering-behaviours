#include "DynamicWander.h"



DynamicWander::DynamicWander(vector<AIObject*> objects)
{
	_offset = 5.f;
	_radius = 3.f;
	_maxAngle = DEGREES_TO_RADIANS(20);
	_angularChange = 0;
	_avoid = new CollisionAvoidance();
	_hook = new GrapplingHook();
	targets = objects;
}

DynamicWander::~DynamicWander()
{
}

SteeringForce DynamicWander::Update(AIObject * _this, vector<AIObject *> targets)
{
	_angularChange = _this->GetKninematik()->GetAngularChange();
	SteeringForce force(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);

	force._linear += (_hook->Update(_this, targets)._linear) ;
	//if (force._linear.Length() >= 0.001f)
		//return force;

	force._linear += (_avoid->Update(_this, this->targets)._linear) * 0.1;
	//if (force._linear.Length() >= 0.001f)
		//return force;


	float orientierung = _this->GetKninematik()->GetOrientierung();

	CHVector middle = CHVector(cos(orientierung), sin(orientierung), _this->GetPos().z) * _offset;

	float p1 = (rand() % 100) / 100.f;
	float p2 = (rand() % 100) / 100.f;
	float p = 1 - (p1 + p2);

	force._angularChange = p * _maxAngle;
	_angularChange += p * _maxAngle;
	CHVector useless = CHVector(cos(_angularChange), sin(_angularChange), .0f);
	_desiredPos = middle + useless.Normal() * _radius;

	force._linear += ((_desiredPos.Normal() * _this->GetKninematik()->GetMaxForce()) *3)*0.5;

	return force;
}
