#include "Game.h"

using namespace AI;

CGame::CGame(void)
{
}

CGame::~CGame(void)
{
}

void CGame::Init(HWND hwnd, void(*procOS)(HWND hwnd, unsigned int uWndFlags), CSplash * psplash)
{
	_root.Init(psplash);
	_camera.Init(QUARTERPI);
	_frame.Init(hwnd, procOS);
	_viewport.InitFull(&_camera);
	_light.Init(CHVector(1.0f, 1.0f, 1.0f), CColor(1.0f, 1.0f, 1.0f));
	srand(static_cast <unsigned> (time(0)));

	_root.AddFrame(&_frame);
	_frame.AddViewport(&_viewport);
	_root.AddScene(&_scene);
	_scene.AddPlacement(&_pCamera);
	_scene.AddLightParallel(&_light);
	_pCamera.AddCamera(&_camera);

	_playgorund = new AIPlayground(5, 2.5, 50,10);
	_scene.AddPlacement(_playgorund);
	_pCamera.RotateXDelta(0.08);
	_pCamera.TranslateZDelta(16.0f);
	_pCamera.TranslateYDelta(-2.f);

	//keyboard
	_frame.AddDeviceKeyboard(&_keyboard);

	

	// Hier die Initialisierung Deiner Vektoria-Objekte einfügen:
}

void CGame::Tick(float fTime, float fTimeDelta)
{
	_root.Tick(fTimeDelta);
	if (_keyboard.KeyDown(DIK_F)) 
		_playgorund->ChangeBehavoiur(FLEE);
	if (_keyboard.KeyDown(DIK_S))
		_playgorund->ChangeBehavoiur(SEEK);
	if (_keyboard.KeyDown(DIK_A))
		_playgorund->ChangeBehavoiur(ARRIVE);
	if (_keyboard.KeyDown(DIK_Q))
		_playgorund->ChangeBehavoiur(D_FLEE);
	if (_keyboard.KeyDown(DIK_W))
		_playgorund->ChangeBehavoiur(D_ARRIVE);
	if (_keyboard.KeyDown(DIK_DOWN))
		_playgorund->ChangeTargets(down);
	if (_keyboard.KeyDown(DIK_UP))
		_playgorund->ChangeTargets(up);
	if (_keyboard.KeyDown(DIK_RIGHT))
		_playgorund->ChangeTargets(random);
	if (_keyboard.KeyDown(DIK_E))
		_playgorund->ChangeBehavoiur(D_WANDER);
	if (_keyboard.KeyDown(DIK_R))
		_playgorund->ChangeBehavoiur(D_FLOCKING);

	_playgorund->Update(fTimeDelta);

	if (fTimeDelta < 12.5f)
		Sleep(12.5f - fTimeDelta);

	// Hier die Echtzeit-Veränderungen einfügen:
}

void CGame::Fini()
{
	// Hier die Finalisierung Deiner Vektoria-Objekte einfügen:
}

void CGame::WindowReSize(int iNewWidth, int iNewHeight)
{
	// Windows ReSize wird immer automatisch aufgerufen, wenn die Fenstergröße verändert wurde.
	// Hier kannst Du dann die Auflösung des Viewports neu einstellen:
}

