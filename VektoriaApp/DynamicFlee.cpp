#include "DynamicFlee.h"



DynamicFlee::DynamicFlee() {}


DynamicFlee::~DynamicFlee() {}

SteeringForce DynamicFlee::Update(AIObject * thisObj, vector<AIObject *> targets) {
	
	if (targets.size() <= thisObj->GetActiveTarget() || thisObj->GetActiveTarget() < 0)
		thisObj->SetTarget(0);

	AIObject* target = targets[thisObj->GetActiveTarget()];
	SteeringForce temp(0, CHVector(0, 0, 0),CHVector(0,0,0),0);
	if (target == nullptr) return temp;

	temp._linear = thisObj->GetPos() - target->GetPos();
	temp._linear = temp._linear.Normal() * thisObj->GetKninematik()->GetMaxForce();
	temp._angular = 0.0f;

	return temp;
}
