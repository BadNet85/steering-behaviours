#include "VelocityMatching.h"



VelocityMatching::VelocityMatching() 
{
	_activationRadius = 1.5f;
}


VelocityMatching::~VelocityMatching() {}

SteeringForce VelocityMatching::Update(AIObject * thisObj, vector<AIObject*> target) {
	SteeringForce force = SteeringForce(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);

	if (target[0] == nullptr) return force;

	CHVector avarageSpeed = CHVector();

	for (size_t i = 0; i < target.size(); i++) {
		CHVector direction = target[i]->GetPos() - thisObj->GetPos();
		float distance = direction.Length();
		if (distance < _activationRadius) {
			avarageSpeed += target[i]->GetKninematik()->GetSpeed();
		}
	}

	force._linear = avarageSpeed;
	return force;
}
