#pragma once
#include "Vektoria/Root.h"
#include "AIObject.h"
#include "AIKugel.h"
#include "SteeringBehaviour.h"
#include "Flee.h"
#include "Seek.h"
#include "Arrive.h"
#include "DynamicFlee.h"
#include "DynamicArrive.h"
#include "DynamicWander.h"
#include "Flocking.h"

using namespace AI;
using namespace std;

class AIPlayground : public CPlacement{

public:
	AIPlayground(int xSize, int ySize, int AIperTeam, int targets);
	void ChangeBehavoiur(BehaviourType type);
	void Update(float fTimeDelta);
	void ChangeTargets(ChangeTargetsDirection direction);
	~AIPlayground();



private:
	std::vector<AIObject*> AIObjects;
	std::vector<AIObject*> TeamA;
	std::vector<AIObject*> TeamB;

	Flee* _flee;
	Seek* _seek;
	Arrive* _arrive;
	DynamicFlee* _dFlee;
	DynamicArrive* _dArrive;
	DynamicWander* _dWander;
	Flocking* _dFlocking;

	CMaterial _AMaterial;
	CMaterial _BMaterial;
	CMaterial _material;
	CGeoQuad _plane;
	CHVector* _size;

	int _selectedTarget;
	int _noOfTargets;
};

