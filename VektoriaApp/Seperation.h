#pragma once
#include "SteeringBehaviour.h"

class Seperation : public SteeringBehaviour{
public:
	Seperation();
	~Seperation();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
private:
	float _activeRadius;
	float FOV;
};

