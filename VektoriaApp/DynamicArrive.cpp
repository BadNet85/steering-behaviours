#include "DynamicArrive.h"



DynamicArrive::DynamicArrive() 
{
	_radius = 1.0f;
}

DynamicArrive::~DynamicArrive() {}

SteeringForce DynamicArrive::Update(AIObject * _this, vector<AIObject *> targets) {
	float desiredSpeed = 0.0f;
	float time = 0.95;
	SteeringForce temp(0, CHVector(0, 0, 0), CHVector(0, 0, 0),0);
	if(targets.size() <= _this->GetActiveTarget() || _this->GetActiveTarget() < 0)
		_this->SetTarget(0);

	AIObject* target = targets[_this->GetActiveTarget()];
	if (target == nullptr) return temp;

	CHVector direction = target->GetPos() - _this->GetPos();

	if (direction.Length() > _radius) 
		desiredSpeed = _this->GetKninematik()->GetMaxSpeed();
	else {
		desiredSpeed = _this->GetKninematik()->GetMaxSpeed()*direction.Length() / (_radius);
	}

	direction = direction.Normal() * desiredSpeed;
	CHVector currentSpeed = _this->GetKninematik()->GetSpeed();
	temp._linear = direction - currentSpeed;
	temp._linear /= time;
	temp._angular = 0.0f;

	return temp;
}
