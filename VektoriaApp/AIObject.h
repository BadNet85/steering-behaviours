#pragma once
#include "Vektoria/Root.h"
#include "Kinematik.h"
#include "SteeringBehaviour.h"
#include "SteeringForce.h"
#include "math.h"
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(radian) ((radian)*180 / M_PI)

enum ChangeTargetsDirection {
	up,
	down,
	random
};

using namespace Vektoria;
using namespace std;

class SteeringBehaviour;

class AIObject : public CPlacement 
{

public:
	AIObject();
	~AIObject();
	void Update(float sizeX, float sizeY, float fTimeDelta);
	Kinematik* GetKninematik();
	void SetTargets(vector<AIObject*> target);
	void SetBehaviour(SteeringBehaviour* behaviour);
	void ChangeTarget(ChangeTargetsDirection direction);
	void SetTarget(int targetID);
	void SetOrientation(float orientation);
	int GetActiveTarget();
	bool GetHookedState();
	void SetHooked(bool state);
	void SetHookedTarget(int target);
	int GetHookedTarget();
private:
	virtual void Move(CHVector direction);
	Kinematik* _kinematik;
	SteeringBehaviour* _myBehaviour;
	vector<AIObject*> _targets;
	int _activeTarget;
	bool _hookedState;
	int _hookedTarget;

};
