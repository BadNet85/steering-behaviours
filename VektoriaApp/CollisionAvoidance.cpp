#include "CollisionAvoidance.h"



CollisionAvoidance::CollisionAvoidance()
{
}

CollisionAvoidance::~CollisionAvoidance()
{
}

SteeringForce CollisionAvoidance::Update(AIObject * thisObj, vector<AIObject*> target)
{
	SteeringForce force(0, CHVector(), CHVector(), 0);
	if (target[0] == nullptr) return force;

	for (size_t i = 0; i < target.size(); i++)
	{
		if ((thisObj->GetPos() - target[i]->GetPos()).Length() <= 0.2f)
		{
			thisObj->SetTarget(i);
			force._linear += (_flee.Update(thisObj, target))._linear;
		}
		else
		{
			CHVector relativePos = target[i]->GetPos() - thisObj->GetPos();
			CHVector realtiveSpeed = target[i]->GetKninematik()->GetSpeed() - thisObj->GetKninematik()->GetSpeed();
			float time = (-relativePos * realtiveSpeed) / ((sqrt(pow(realtiveSpeed.x, 2) + pow(realtiveSpeed.y, 2)) * (sqrt(pow(realtiveSpeed.x, 2) + pow(realtiveSpeed.y, 2)))));

			if (time > 0)
			{
				CHVector thisPropDist = thisObj->GetPos() + thisObj->GetKninematik()->GetSpeed() * time;
				CHVector otherPropDist = target[i]->GetPos() + target[i]->GetKninematik()->GetSpeed() * time;

				if ((otherPropDist - thisPropDist).Length() <= 0.4f)
				{
					thisObj->SetTarget(i);
					force._linear += (_flee.Update(thisObj, target))._linear;
				}
			}
		}
	}
	return force;
}
