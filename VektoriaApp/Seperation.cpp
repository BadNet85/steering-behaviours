#include "Seperation.h"



Seperation::Seperation() 
{
	_activeRadius = 1.25f;
	FOV = DEGREES_TO_RADIANS(360);
}


Seperation::~Seperation() {}

SteeringForce Seperation::Update(AIObject * thisObj, vector<AIObject*> target) {

	SteeringForce force = SteeringForce(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);
	if (target[0] == nullptr) return force;
	for (size_t i = 0; i < target.size(); i++) {
		CHVector toMe = thisObj->GetPos() - target[i]->GetPos();
		CHVector toTarget = target[i]->GetPos() - thisObj->GetPos();
		float distance = toMe.Length();
		if (distance < _activeRadius && visable(thisObj->GetKninematik()->GetSpeed(),toTarget,FOV)) {
			float linearForce = thisObj->GetKninematik()->GetMaxForce()*(_activeRadius - distance) / _activeRadius;
			force._linear += toMe.Normal() * linearForce;
		}
	}
	return force;
}
