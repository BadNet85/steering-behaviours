#include "Kinematik.h"


Kinematik::Kinematik() {
	_maxSpeed = 1.f;
	_maxForce = 1.0f;
	_orientation = 0.f;
}


Kinematik::~Kinematik() {}

void Kinematik::Update(SteeringForce force, float fTimeDelta) 
{
	CHVector temp = CHVector();

	if (force._linear.Length() != 0) {
		_moveSpeed += (force._linear * fTimeDelta);
	}
	else
	{
		if (force._kinematikLinear.x == 0 && force._kinematikLinear.y == 0 && force._kinematikLinear.z == 0)
			_moveSpeed.SetXYZ(0);
		_moveSpeed = force._kinematikLinear;
		_orientation = force._kinematikAngular;
	}

	if (_moveSpeed.Length() > _maxSpeed)
		_moveSpeed = _moveSpeed.Normal()* _maxSpeed;

	if (_moveSpeed.Length() != 0)
	{
		float tempOrient = acos(_moveSpeed.x / sqrt(pow(_moveSpeed.x, 2) + pow(_moveSpeed.y, 2)));
		if (_moveSpeed.y >= 0)
			_orientation = tempOrient;
		else
			_orientation = -tempOrient;

		_angularChange += force._angularChange;
	}
	int i = 0;
}

float Kinematik::GetMaxSpeed() {
	return _maxSpeed;
}

CHVector Kinematik::GetSpeed() {
	return _moveSpeed;
}

float Kinematik::GetRotSpeed() {
	return _rotSpeed;
}

float Kinematik::GetMaxForce() {
	return _maxForce;
}

void Kinematik::SetOrientation(float orientation)
{
	this->_orientation = orientation;
	_angularChange = orientation;
}

float Kinematik::GetOrientierung()
{
	return _orientation;
}

float Kinematik::GetAngularChange()
{
	return _angularChange;
}

void Kinematik::SetSpeed(CHVector speed)
{
	_moveSpeed = speed;
}
