#include "SteeringBehaviour.h"

SteeringBehaviour::SteeringBehaviour() {}


SteeringBehaviour::~SteeringBehaviour() {}

SteeringForce SteeringBehaviour::Update(AIObject* thisObj, vector<AIObject*> target)
{
	SteeringForce temp(0, CHVector(0, 0, 0),CHVector(0,0,0),0);
	return temp;
}

bool SteeringBehaviour::visable(CHVector & orientation, CHVector & destination, float FOV) {
	CHVector c = orientation;
	CHVector z = destination;
	float relativeAngle = acos((c*z) / (c.Length()*z.Length()));
	if (relativeAngle < 0.5f*FOV)
		return true;
	else
		return false;
}
