#include "Flee.h"



Flee::Flee() {}

SteeringForce Flee::Update(AIObject* _this, vector<AIObject *> targets) {

	if (targets.size() <= _this->GetActiveTarget() || _this->GetActiveTarget() < 0)
		_this->SetTarget(0);

	AIObject* target = targets[_this->GetActiveTarget()];
	SteeringForce temp(0, CHVector(0, 0, 0),CHVector(0,0,0),0);
	if (target == nullptr) return temp;

	CHVector newSpeed = _this->GetPos() - target->GetPos();
	newSpeed = newSpeed.Normal() * _this->GetKninematik()->GetMaxSpeed();

	temp._kinematikLinear = newSpeed;

	return temp;
}

Flee::~Flee() {}
