#pragma once
#include "SteeringBehaviour.h"


class Seek : public SteeringBehaviour
{
public:
	Seek();
	~Seek();
	SteeringForce Update(AIObject* _this, vector<AIObject *> targets);
};

