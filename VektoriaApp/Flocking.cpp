#include "Flocking.h"



Flocking::Flocking(vector<AIObject*> buddies)
{
	_cohersion = new Cohersion();
	_seperation = new Seperation();
	_matching = new VelocityMatching();
	_weightCoher = 1.0f;
	_weightSeper = 1.0f;
	_weightVeloc = 1.0f;
	this->buddies = buddies;
}


Flocking::~Flocking()
{
}

SteeringForce Flocking::Update(AIObject * thisObj, vector<AIObject*> target)
{
	SteeringForce force = SteeringForce(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);

	if (target[0] == nullptr) return force;

	force._linear = CHVector(0, 0, 0);
	force._angular = 0.0f;

	force._linear += _cohersion->Update(thisObj, buddies)._linear *_weightCoher;
	force._linear += _seperation->Update(thisObj, buddies)._linear * _weightSeper;
	force._linear += _matching->Update(thisObj, buddies)._linear * _weightVeloc;

	float tempOrient = acos(force._linear.x / sqrt(pow(force._linear.x, 2) + pow(force._linear.y, 2)));
	if (force._linear.y >= 0)
		force._kinematikAngular = tempOrient;
	else
		force._kinematikAngular = -tempOrient;


	return force;
}