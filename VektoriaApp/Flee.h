#pragma once
#include "SteeringBehaviour.h"
class Flee : public SteeringBehaviour {

public:
	Flee();
	SteeringForce Update(AIObject* _this, vector<AIObject *> targets);
	~Flee();
};

