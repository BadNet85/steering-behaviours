#pragma once
#include "SteeringBehaviour.h"
class DynamicArrive : public SteeringBehaviour
{
public:
	DynamicArrive();
	~DynamicArrive();
	SteeringForce Update(AIObject* _this, vector<AIObject *> targets);
	float _radius;
};

