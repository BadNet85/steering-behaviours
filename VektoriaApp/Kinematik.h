#pragma once
#include "SteeringForce.h"
#include "Vektoria/Root.h"
#include "Math.h"

using namespace Vektoria;

class Kinematik {
public:
	Kinematik();
	~Kinematik();
	void Update(SteeringForce force, float fTimeDelta);
	float GetMaxSpeed();
	CHVector GetSpeed();
	float GetRotSpeed();
	float GetMaxForce();
	void SetOrientation(float orientation);
	float GetOrientierung();
	float GetAngularChange();
	void SetSpeed(CHVector speed);

private:
	float _orientation;
	CHVector _moveSpeed;
	float _rotSpeed;
	float _maxSpeed;
	float _maxForce;
	float _angularChange;
};