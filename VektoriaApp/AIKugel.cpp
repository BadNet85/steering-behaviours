#include "AIKugel.h"

using namespace AI;

AIKugel::AIKugel(float radius) 
{

	_sphere.Init(radius, NULL);
	_head.Init(radius/2, NULL);
	this->AddGeo(&_sphere);
	this->AddPlacement(&_pHead);
	_pHead.Translate(radius,0,0);
	_pHead.AddGeo(&_head);
}


AIKugel::~AIKugel() {}

void AIKugel::Move(CHVector direction) {

}

void AI::AIKugel::SetMaterial(CMaterial* _material)
{
	_sphere.SetMaterial(_material);
}
