#include "AIObject.h"


AIObject::AIObject() 
{
	_kinematik = new Kinematik();
	_myBehaviour = nullptr;
	_activeTarget = 0;
	_hookedState = false;
	_hookedTarget = 0;
}
AIObject::~AIObject() {}

void AIObject::Update(float sizeX, float sizeY, float fTimeDelta)
{
	if (_myBehaviour == nullptr) return;

	_kinematik->Update(_myBehaviour->Update(this, _targets), fTimeDelta);

	//RotateYDelta(_kinematik->GetRotSpeed());

	TranslateDelta(_kinematik->GetSpeed()*fTimeDelta);



	//Spielfeldgrenzen
	CHVector temp = this->GetPos();
	float orientation = _kinematik->GetOrientierung();

	if (temp.x > sizeX)
	{
		temp.x = sizeX;
		_kinematik->SetOrientation(PI - _kinematik->GetOrientierung());
		_kinematik->SetSpeed(CHVector(cos(_kinematik->GetOrientierung()), sin(_kinematik->GetOrientierung()), GetPos().z));
	}
	else if (temp.x < -sizeX)
	{
		temp.x = -sizeX;
		if (_kinematik->GetOrientierung() >= 0)
		{
			_kinematik->SetOrientation(PI - _kinematik->GetOrientierung());
		}
		else
		{
			_kinematik->SetOrientation(-PI - _kinematik->GetOrientierung());
		}
		_kinematik->SetSpeed(CHVector(cos(_kinematik->GetOrientierung()), sin(_kinematik->GetOrientierung()), GetPos().z));
	}
	if (temp.y > sizeY)
	{
		temp.y = sizeY;
		_kinematik->SetOrientation(0 - _kinematik->GetOrientierung());
		_kinematik->SetSpeed(CHVector(cos(_kinematik->GetOrientierung()), sin(_kinematik->GetOrientierung()), GetPos().z));
	}
	else if (temp.y < -sizeY)
	{
		temp.y = -sizeY;
		_kinematik->SetOrientation(0 - _kinematik->GetOrientierung());
		_kinematik->SetSpeed(CHVector(cos(_kinematik->GetOrientierung()), sin(_kinematik->GetOrientierung()), GetPos().z));
	}

	this->RotateZ(_kinematik->GetOrientierung());
	this->TranslateDelta(temp);
}

Kinematik* AIObject::GetKninematik() 
{
	return _kinematik;
}
void AIObject::SetTargets(vector<AIObject*> target) 
{
	_targets = target;
}
void AIObject::SetBehaviour(SteeringBehaviour * behaviour) 
{
	_myBehaviour = behaviour;
}
void AIObject::ChangeTarget(ChangeTargetsDirection direction) 
{
	switch (direction) {
		case(up):
		{
			_activeTarget++;
			break;
		}
		case(down):
		{
			_activeTarget--;
			break;
		}
		case(random):
		{
			_activeTarget = static_cast <int> (rand()) / (static_cast <int> (RAND_MAX / _targets.size()));
			break;
		}
	}
	if (_activeTarget > _targets.size() - 1)
		_activeTarget = _targets.size() - 1;
	if (_activeTarget < 0)
		_activeTarget = 0;
}

void AIObject::SetTarget(int targetID)
{
	_activeTarget = targetID;
}
void AIObject::SetOrientation(float orientation)
{
	_kinematik->SetOrientation(orientation);
}
int AIObject::GetActiveTarget() {
	return _activeTarget;
}
bool AIObject::GetHookedState()
{
	return _hookedState;
}
void AIObject::SetHooked(bool state)
{
	_hookedState = state;
}
void AIObject::SetHookedTarget(int target)
{
	_hookedTarget = target;
}
int AIObject::GetHookedTarget()
{
	return _hookedTarget;
}
void AIObject::Move(CHVector direction) {}


