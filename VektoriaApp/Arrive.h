#pragma once
#include "SteeringBehaviour.h"
class Arrive : public SteeringBehaviour
{
public:
	Arrive();
	~Arrive();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> targets);
private:
	float _radius;
};

