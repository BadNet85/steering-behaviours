#include "Arrive.h"



Arrive::Arrive() 
{
	_radius = 0.1f;
}


Arrive::~Arrive() {}

SteeringForce Arrive::Update(AIObject * thisObj, vector<AIObject *> targets) {

	SteeringForce force(0, CHVector(0, 0, 0), CHVector(0, 0, 0), 0);

	if (targets.size() <= thisObj->GetActiveTarget() || thisObj->GetActiveTarget() < 0)
		thisObj->SetTarget(0);

	AIObject* target = targets[thisObj->GetActiveTarget()];

	if (target == nullptr) return force;

	CHVector temp = target->GetPos() - thisObj->GetPos();

	if (temp.Length() <= _radius) 
	{
		return force;
	}
	else 
	{
		temp = temp.Normal()*thisObj->GetKninematik()->GetMaxSpeed();

		force._kinematikLinear = temp;

		return force;
	}


}
