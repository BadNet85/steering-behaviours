#include "Seek.h"



Seek::Seek() {}


Seek::~Seek() {}

SteeringForce Seek::Update(AIObject* _this, vector<AIObject *> targets)
{
	AIObject* target = targets[_this->GetActiveTarget()];
	SteeringForce temp(0, CHVector(0, 0, 0),CHVector(0,0,0),0);
	if (target == nullptr) return temp;

	CHVector speed = target->GetPos() - _this->GetPos();
	speed = speed.Normal()* _this->GetKninematik()->GetMaxSpeed();
	temp._kinematikLinear = speed;


	return temp;

}
