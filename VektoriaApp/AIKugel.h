#pragma once
#include "Vektoria/Root.h"
#include "AIObject.h"

namespace AI
{
class AIKugel : public AIObject {

public:
	AIKugel(float radius);
	~AIKugel();
	void Move(CHVector direction);
	void SetMaterial(CMaterial* _material);

private:
	CGeoSphere _sphere;
	CPlacement _pHead;
	CGeoSphere _head;

};
}