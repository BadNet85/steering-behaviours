#pragma once
#include "SteeringBehaviour.h"
#include "CollisionAvoidance.h"
#include "GrapplingHook.h"

class DynamicWander : public SteeringBehaviour
{
public:
	DynamicWander(vector<AIObject*> objects);
	~DynamicWander();
	SteeringForce Update(AIObject* _this, vector<AIObject *> targets);
private:
	float _offset;
	float _radius;
	CHVector _desiredPos;
	float _maxAngle;
	float _angularChange;
	bool firstRun;
	std::vector<AIObject *> targets;
	CollisionAvoidance* _avoid;
	GrapplingHook* _hook;
};

