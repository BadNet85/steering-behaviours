#include "AIPlayground.h"
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)


AIPlayground::AIPlayground(int xSize, int ySize, int AIperTeam, int targets)
{
	//Materialien der Objekte
	_material.MakeTextureDiffuse("textures\\green_image.jpg");
	_AMaterial.MakeTextureDiffuse("textures\\red_image.jpg");
	_BMaterial.MakeTextureDiffuse("textures\\blue_image.jpg");
	
	//Plane als Spielfeld
	_plane.Init(xSize, ySize, &_material);
	this->AddGeo(&_plane);

	//Gr��e f�r einfaches verwenden in Vektor speichern
	_size = new CHVector(xSize, ySize, 0);
	_noOfTargets = targets;
	
	//Ziele der A und B Teams
	vector<AIObject*> _teamADest;
	vector<AIObject*> _teamBDest;

	//Pointer auf Behaviours
	//TODO: In IAObjects Verschieben
	_flee = new Flee();
	_seek = new Seek();
	_arrive = new Arrive();
	_dFlee = new DynamicFlee();
	_dArrive = new DynamicArrive();

	int teamsInitialized = 0;
	while (teamsInitialized < 2) 
	{
		int targetsInit = 0;
		for (int i = 0; i < AIperTeam+targets; i++) {
			AIKugel* _this = new AIKugel(0.1);

			if(targetsInit < targets/2)
			{
				_teamADest.push_back(_this);
				this->AddPlacement(_this);
				targetsInit++;
			}
			else if(targetsInit < targets)
			{
				_teamBDest.push_back(_this);
				this->AddPlacement(_this);
				targetsInit++;
			}

			else if (teamsInitialized < 1) 
			{
				_this->SetMaterial(&_AMaterial);
				_this->SetTargets(_teamADest);
				TeamA.push_back(_this);
				AIObjects.push_back(_this);
			}
			else 
			{
				_this->SetMaterial(&_BMaterial);
				_this->SetTargets(_teamBDest);
				TeamB.push_back(_this);
				AIObjects.push_back(_this);
			}
			this->AddPlacement(_this);
			float orientation = DEGREES_TO_RADIANS(static_cast <float> (rand()) / static_cast <float> (RAND_MAX / 360));
			_this->RotateZDelta(orientation);
			_this->SetOrientation(orientation);
			_this->TranslateDelta(-(xSize),-(ySize),0);
			float x = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / xSize*0.5));
			float y = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / ySize*0.5));
			_this->TranslateDelta(x, y, 0);
		}
		teamsInitialized++;
	}
	_dFlocking = new Flocking(AIObjects);
	_dWander = new DynamicWander(AIObjects);
}

void AIPlayground::ChangeBehavoiur(BehaviourType type)
{
	int p = (int)AIObjects.size();
	for (int i = 0; i < AIObjects.size(); i++) {
		switch (type) {
			case(FLEE):
			{
				AIObjects[i]->SetBehaviour(_flee);
				break;
			}
			case(SEEK):
			{
				AIObjects[i]->SetBehaviour(_seek);
				break;
			}
			case(ARRIVE):
			{
				AIObjects[i]->SetBehaviour(_arrive);
				break;
			}
			case(D_FLEE):
			{
				AIObjects[i]->SetBehaviour(_dFlee);
				break;
			}
			case(D_ARRIVE):
			{
				AIObjects[i]->SetBehaviour(_dArrive);
				break;
			}
			case(D_WANDER):
			{
				AIObjects[i]->SetBehaviour(_dWander);
				break;
			}
			case(D_FLOCKING):
			{
				AIObjects[i]->SetBehaviour(_dFlocking);
				break;
			}
		}
	}
}

void AIPlayground::Update(float fTimeDelta) 
{
	for (int i = 0; i < AIObjects.size(); i++) {
		AIObjects[i]->Update(_size->x, _size->y, fTimeDelta);
	}
}

void AIPlayground::ChangeTargets(ChangeTargetsDirection direction) 
{
	for (int i = 0; i < AIObjects.size(); i++)
		AIObjects[i]->ChangeTarget(direction);

}

AIPlayground::~AIPlayground() {}
