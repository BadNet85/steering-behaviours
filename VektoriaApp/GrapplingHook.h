#pragma once
#include "SteeringBehaviour.h"
class GrapplingHook : public SteeringBehaviour
{
private:
	std::vector<CHVector*> _path;
	float arrivalDist;
	int _currentTarget;
public:
	GrapplingHook();
	~GrapplingHook();
	SteeringForce Update(AIObject* thisObj, vector<AIObject*> target);
};

