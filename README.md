# Dynamic AI movement Showcase

Simple interactive AI movement showcase using the Vectoria Engine of the University of Applied Sience Kempten (http://games.hs-kempten.de/vektoria-download/).


There are two different movement calculation modes: dynamic and kinematic

    while kinematic does not take forces into considertion and just set the current movement speed to what is disired, the dynamic mode uses steering forces to calculate movement of
    the chracters.
    
Movement modes are:
    Wander: move aimless around the playground
    Flee: Flee from current target
    Seek: go to current target
    arrive: arrive (stop) at current target
    flock: keep formation with other characters

### Prerequisites

Microsoft DirectX11 SDK needs to be installed.

	ERROR S1203:

	In Commandline:
	MsiExec.exe /passive /X{F0C3E5D1-1ADE-321E-8167-68EF0DE699A5} 
	and 
	MsiExec.exe /passive /X{1D8E6291-B0D5-35EC-8441-6616F567A0F7}

Windows-SDK version 10.0.10240.0 

### Installing

Download the Vectoria Engine Library (http://games.hs-kempten.de/vektoria-download/) and place the Lib into the same folder with VektoriaApp.sln

### Controls

    key             function
    f               Flee(Kinematic)
    s               Seek(Kinematic)
    a               Arrive(kinematic)
    q               Flee(dynamic)
    w               Arrive(dynamic)
    e               Wander(dynamic)
    r               Flock(dynamic)
    up arrow        Next Target
    down arrow      Prev Target
    right arrow     random Target
    